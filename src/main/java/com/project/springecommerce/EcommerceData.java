package com.project.springecommerce;

import com.project.springecommerce.Config.PasswordEncoder;
import com.project.springecommerce.Repository.Cart;
import com.project.springecommerce.Repository.Product;
import com.project.springecommerce.Repository.User;
import com.project.springecommerce.Service.CartService;
import com.project.springecommerce.Service.ProductService;
import com.project.springecommerce.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

@Component
public class EcommerceData implements CommandLineRunner {

    @Autowired
    private UserService userService;
    @Autowired
    private ProductService productService;
    @Autowired
    private CartService cartService;
    private final ArrayList<User> defaultUserData = new ArrayList<>();
    private final ArrayList<Product> defaultProductData = new ArrayList<>();

    @Override
    public void run(String... args) throws Exception {
        populateUserTable();
        populateProductTable();
        populateCartTable();
    }

    private void populateUserTable() throws NoSuchAlgorithmException {
        User user1 = new User();
        user1.setFirstName("Tyler");
        user1.setLastName("Durden");
        user1.setEmail("tylerdurden@gmail.com");
        user1.setUsername("tylerdurden");
        user1.setRole("seller");
        user1.setPassword(PasswordEncoder.encode("tyler123"));
        userService.save(user1);

        User user2 = new User();
        user2.setFirstName("Patrick");
        user2.setLastName("Bateman");
        user2.setEmail("patrickbateman@hotmail.com");
        user2.setUsername("patrickbateman");
        user2.setRole("seller");
        user2.setPassword(PasswordEncoder.encode("patrick123"));
        userService.save(user2);

        User user3 = new User();
        user3.setFirstName("Travis");
        user3.setLastName("Bickle");
        user3.setEmail("travisbickle@yahoo.com");
        user3.setUsername("travisbickle");
        user3.setRole("buyer");
        user3.setPassword(PasswordEncoder.encode("travis123"));
        userService.save(user3);

        User user4 = new User();
        user4.setFirstName("Donnie");
        user4.setLastName("Darko");
        user4.setEmail("donniedarko@outlook.com");
        user4.setUsername("donniedarko");
        user4.setRole("buyer");
        user4.setPassword(PasswordEncoder.encode("donnie123"));
        userService.save(user4);

        defaultUserData.addAll(userService.findAll());
    }

    private void populateProductTable() {
        Product product1 = new Product();
        product1.setName("Cinema Camera");
        product1.setPrice("5000");
        product1.setQuantity(10);
        product1.setDescription("Professional-grade camera for high-quality filmmaking.");
        product1.setSeller(defaultUserData.get(0));
        productService.save(product1);

        Product product2 = new Product();
        product2.setName("Cine Lens");
        product2.setPrice("3000");
        product2.setQuantity(5);
        product2.setDescription("Premium lens designed for cinematic visuals.");
        product2.setSeller(defaultUserData.get(1));
        productService.save(product2);

        Product product3 = new Product();
        product3.setName("Film Lighting Kit");
        product3.setPrice("2000");
        product3.setQuantity(3);
        product3.setDescription("Complete lighting setup for professional film production.");
        product3.setSeller(defaultUserData.get(1));
        productService.save(product3);

        Product product4 = new Product();
        product4.setName("Camera Stabilizer");
        product4.setPrice("1000");
        product4.setQuantity(8);
        product4.setDescription("Tool for capturing steady and smooth shots.");
        product4.setSeller(defaultUserData.get(0));
        productService.save(product4);

        Product product5 = new Product();
        product5.setName("Audio Recorder");
        product5.setPrice("800");
        product5.setQuantity(12);
        product5.setDescription("High-quality recorder for capturing crystal-clear sound.");
        product5.setSeller(defaultUserData.get(0));
        productService.save(product5);

        defaultProductData.addAll(productService.findAll());
    }

    private void populateCartTable() {
        Cart cart1 = new Cart();
        cart1.setQuantity(2);
        cart1.setBuyer(defaultUserData.get(2));
        cart1.setProduct(defaultProductData.get(3));
        cartService.save(cart1);

        Cart cart2 = new Cart();
        cart2.setQuantity(1);
        cart2.setBuyer(defaultUserData.get(3));
        cart2.setProduct(defaultProductData.get(4));
        cartService.save(cart2);
    }


}

