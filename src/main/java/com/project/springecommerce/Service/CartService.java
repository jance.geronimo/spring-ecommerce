package com.project.springecommerce.Service;

import com.project.springecommerce.Repository.Cart;
import com.project.springecommerce.Repository.User;

import java.util.List;

public interface CartService {
    Cart save(Cart cart);

    List<Cart> findAll();

    Cart findById(long id);

    void deleteById(long id);

    Cart updateById(long id, Cart cart);

    List<Cart> findByBuyer(User user);
}
