package com.project.springecommerce.Service;

import com.project.springecommerce.Exception.BadRequestException;
import com.project.springecommerce.Repository.User;
import com.project.springecommerce.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImplementation implements UserService {

    @Autowired
    UserRepository userRepository;
    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(long id) {
        return userRepository.findById(id).orElseThrow(() -> new BadRequestException("User does not exist"));
    }

    @Override
    public void deleteById(long id) {
        userRepository.deleteById(id);
    }

    @Override
    public User updateById(long id, User user) {
        user.setId(id);
        return userRepository.save(user);
    }
    //Additional
    @Override
    public User findByUsername(String userName) {
        return userRepository.findByUsername(userName);
    }
}

