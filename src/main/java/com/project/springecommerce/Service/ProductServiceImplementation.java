package com.project.springecommerce.Service;

import com.project.springecommerce.Exception.BadRequestException;
import com.project.springecommerce.Repository.Product;
import com.project.springecommerce.Repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImplementation implements  ProductService{

    @Autowired
    ProductRepository productRepository;
    @Override
    public Product save(Product product) {
        return productRepository.save(product);
    }

    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @Override
    public Product findById(long id) {
        return productRepository.findById(id).orElseThrow(() -> new BadRequestException("Product does not exist"));
    }

    @Override
    public void deleteById(long id) {
        productRepository.deleteById(id);
    }

    @Override
    public Product updateById(long id, Product product) {
        product.setId(id);
        return productRepository.save(product);
    }
}

