package com.project.springecommerce.Service;

import com.project.springecommerce.Exception.BadRequestException;
import com.project.springecommerce.Repository.Cart;
import com.project.springecommerce.Repository.CartRepository;
import com.project.springecommerce.Repository.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CartServiceImplementation implements CartService{
    @Autowired
    CartRepository cartRepository;
    @Override
    public Cart save(Cart cart) {
        return cartRepository.save(cart);
    }

    @Override
    public List<Cart> findAll() {
        return cartRepository.findAll();
    }

    @Override
    public Cart findById(long id) {
        return cartRepository.findById(id).orElseThrow(() -> new BadRequestException("Cart does not exist"));
    }

    @Override
    public void deleteById(long id) {
        cartRepository.deleteById(id);
    }

    @Override
    public Cart updateById(long id, Cart cart) {
        cart.setId(id);
        return cartRepository.save(cart);
    }

    @Override
    public List<Cart> findByBuyer(User user) {
        return cartRepository.findByBuyer(user);
    }
}
