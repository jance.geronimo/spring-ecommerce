package com.project.springecommerce.Service;

import com.project.springecommerce.Repository.Product;

import java.util.List;

public interface ProductService {
    Product save(Product product);

    List<Product> findAll();

    Product findById(long id);

    void deleteById(long id);

    Product updateById(long id, Product product);
}
