package com.project.springecommerce.Controller;

import com.project.springecommerce.Repository.Cart;
import com.project.springecommerce.Service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/cart")
public class CartController {
    @Autowired
    CartService cartService;

    @PostMapping("/add")
    public Cart addCart(@RequestBody Cart cart) {
        return cartService.save(cart);
    }

    @GetMapping("/getAll")
    public List<Cart> getAllCart() {
        return cartService.findAll();
    }

    @GetMapping("/getById")
    public Cart getById(@RequestParam long id) {
        return cartService.findById(id);
    }

    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@PathVariable long id) {
        cartService.deleteById(id);
    }

    @PutMapping("/updateById/{id}")
    public Cart updateById(@PathVariable long id, @RequestBody Cart cart) {
        return cartService.updateById(id, cart);
    }
}
