package com.project.springecommerce.Controller;

import com.project.springecommerce.Config.PasswordEncoder;
import com.project.springecommerce.Repository.Cart;
import com.project.springecommerce.Repository.Product;
import com.project.springecommerce.Repository.User;
import com.project.springecommerce.Service.CartService;
import com.project.springecommerce.Service.ProductService;
import com.project.springecommerce.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Controller
public class UserInterfaceController {
    @Autowired
    private UserService userService;

    @Autowired
    private CartService cartService;

    @Autowired
    private ProductService productService;

    @GetMapping("/home")
    public String home_get(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();

        User user = userService.findByUsername(username);
        System.out.println("Username: " + username);
        model.addAttribute("user", user);

        List<Product> product = productService.findAll();
        model.addAttribute("products", product);

        return "home";
    }

    @GetMapping("/login")
    public String index_get() {
        return "login";
    }

    @GetMapping("/cart")
    @PreAuthorize("hasRole('BUYER')")
    public String showCart(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        User user = userService.findByUsername(username);

        List<Cart> cart = cartService.findByBuyer(user);
        System.out.println("Cart: " + cart.toString());
        model.addAttribute("cart", cart);

        int totalPrice = 0;
        int totalQuantity = 0;
        for (int i = 0; i < cart.size(); i++) {
            int price = Integer.parseInt(cart.get(i).getProduct().getPrice());
            int quantity = cart.get(i).getQuantity();
            totalPrice += (price * quantity);
            totalQuantity += quantity;
        }
        model.addAttribute("totalPrice", totalPrice);
        model.addAttribute("totalItems", totalQuantity);


        return "cart";
    }

    @PostMapping("/cart/edit")
    @PreAuthorize("hasRole('BUYER')")
    public String editCart(@RequestParam("id") String id, @RequestParam("action") String action) {
        Long long_id = Long.parseLong(id);
        if (action.equalsIgnoreCase("subtract")) {
            Cart cart = cartService.findById(long_id);
            int quantity = cart.getQuantity() - 1;
            cart.setQuantity(quantity);
            cartService.updateById(long_id, cart);
        } else if (action.equalsIgnoreCase("add")) {
            Cart cart = cartService.findById(long_id);
            int quantity = cart.getQuantity() + 1;
            cart.setQuantity(quantity);
            cartService.updateById(long_id, cart);
        } else {
            Cart cart = cartService.findById(long_id);
            cartService.deleteById(cart.getId());
        }

        return "redirect:/cart";
    }

    @GetMapping("/signup")
    public String showSignup() {
        return "signup";
    }

    @PostMapping("/signup")
    public String validateSignup(@RequestParam Map<String, String> formData, Model model) {

        String password = formData.get("password");
        String confirmPassword = formData.get("confirmPassword");

        if (password.equals(confirmPassword)) {
            User user = new User();
            user.setFirstName(formData.get("firstName"));
            user.setLastName(formData.get("lastName"));
            user.setEmail(formData.get("email"));
            user.setRole(formData.get("role"));
            user.setUsername(formData.get("username"));
            user.setPassword(PasswordEncoder.encode("password"));
            user.setEnabled(true);
            userService.save(user);
            return "redirect:signup?complete";
        }

        return "redirect:signup?PasswordsNotMatch";
    }

    @GetMapping("/product/edit/{id}")
    @PreAuthorize("hasRole('SELLER')")
    public String showCrudProduct_Edit(@PathVariable("id") long id, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        User user = userService.findByUsername(username);

        Product product = productService.findById(id);

        if (product.getSeller().getId() == user.getId()) {
            model.addAttribute("product", product);
            model.addAttribute("action", "EDIT");
            return "crudProduct";
        }

        return "redirect:/home";
    }

    @PostMapping("/product/update")
    @PreAuthorize("hasRole('SELLER')")
    public String updateCrudProduct(@ModelAttribute Product product, @RequestParam("action") String action) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        User user = userService.findByUsername(username);
//
//        if (user.getId() != product.getSeller().getId()) {
//            return "redirect:/home";
//        }

        if (action.equalsIgnoreCase("save")) {
            product.setSeller(user);
            System.out.println(product.toString());
            productService.save(product);

            String productId = String.valueOf(product.getId());

            return "redirect:/product/edit/" + productId + "?success";
        }

        productService.deleteById(product.getId());
        return "redirect:/home";
    }

    @GetMapping("/product/add")
    @PreAuthorize("hasRole('SELLER')")
    public String showCrudProduct_Save(Model model) {
        model.addAttribute("action", "ADD");
        return "crudProduct";
    }

    @PostMapping("/product/save")
    @PreAuthorize("hasRole('SELLER')")
    public String saveCrudProduct(@ModelAttribute Product product) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        User user = userService.findByUsername(username);

        product.setSeller(user);

        System.out.println(product.toString());
        productService.save(product);

        return "redirect:/product/add?success";
    }

    @GetMapping("/product/view/{id}")
    public String showViewProduct(@PathVariable("id") long id, Model model) {
        Product product = productService.findById(id);
        model.addAttribute("product", product);

        return "viewProduct";
    }

    @PostMapping("/product/view/{id}")
    public String addToCart(@PathVariable("id") long id) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        User user = userService.findByUsername(username);

        if (user == null) {
            return "redirect:/login?cart";
        }

        Product product = productService.findById(id);

        Cart cart = new Cart();
        boolean hasCartContents = false;
        List<Cart> cartList = cartService.findByBuyer(user);
        if (cartList.size() != 0) {
            for (int i = 0; i < cartList.size(); i++) {
                if (cartList.get(i).getProduct().getId()==product.getId()){
                    cart = cartList.get(i);
                    int quantity = cartList.get(i).getQuantity() + 1;
                    cart.setQuantity(quantity);
                    cartService.updateById(cart.getId(), cart);
                    hasCartContents = true;
                }
            }
        }
        if(!hasCartContents){
            cart.setQuantity(1);
            cart.setProduct(product);
            cart.setBuyer(user);
            cartService.save(cart);
        }


        String redirectURL = "redirect:/product/view/"
                + product.getId()
                + "?cartSuccess";
        return redirectURL;
    }
}