package com.project.springecommerce.Repository;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table
@Setter
@Getter
@ToString
public class Cart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private int quantity;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "fk_product_id"
            , referencedColumnName = "id"
            , foreignKey = @ForeignKey(name = "FK_Product_Cart"
            , value = ConstraintMode.PROVIDER_DEFAULT))
    private Product product;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "fk_user_id"
            , referencedColumnName = "id"
            , foreignKey = @ForeignKey(name = "FK_User_Cart"
            , value = ConstraintMode.PROVIDER_DEFAULT))
    private User buyer;

}