package com.project.springecommerce.Repository;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table
@Setter
@Getter
@ToString
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String price;
    private int quantity;
    private String description;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "fk_user_id"
            , referencedColumnName = "id"
            , foreignKey = @ForeignKey(name = "FK_User_Product"
            , value = ConstraintMode.PROVIDER_DEFAULT))
    private User seller;

}
